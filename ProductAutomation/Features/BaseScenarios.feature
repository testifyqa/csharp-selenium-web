@Chrome
Feature: BaseScenarios

These scenarios can be used in any project

Background: DuckDuckGo user is already on the correct page
 Given a DuckDuckGo user is on the base page

Scenario: 01. Validate the title of a website
 Then they see the page title contains "DuckDuckGo"

Scenario: 02. Validate the URL of a web page
 Then the page URL contains "https://start.duckduckgo.com"

Scenario: 03. Validate the PageSource string on a web page
 Then they see "DuckDuckGo" in the PageSource

Scenario: 04. Validate the PageSource contains multiple text
 Then they see
  | expectedText  |
  | DuckDuckGo    |
  | search engine |
  | track you     |