using OpenQA.Selenium;

namespace ProductAutomation.Pages
{
    public class RedditPage : SearchResultsPage
    {
        public By redditContentArea = By.Id("SHORTCUT_FOCUSABLE_DIV");
    }
}