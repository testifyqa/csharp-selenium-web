using System.Collections.Generic;
using OpenQA.Selenium;
using ProductAutomation.Utils.Extensions;

namespace ProductAutomation.Pages
{
    public class SearchResultsPage : BasePage
    {
        public By SearchResultsContainer => By.Id("links");
        public By SearchResultLinks => By.ClassName("result__a");

        public RedditPage SelectFirstListedSearchResult()
        {
            SearchResultLinks.WdClickByIndex(0);
            return new RedditPage();
        }
    }
}