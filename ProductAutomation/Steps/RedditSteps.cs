using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using ProductAutomation.Pages;
using ProductAutomation.Utils.Extensions;
using TechTalk.SpecFlow;

namespace ProductAutomation.Steps.RedditSteps
{
    [Binding]
    public class RedditSteps
    {
        private SearchResultsPage searchResultsPage = new SearchResultsPage();
        private RedditPage redditPage = new RedditPage();

        [When(@"they view the first result")]
        public void WhenIViewTheFirstResult()
        {
            IWebElement searchResults = searchResultsPage.driver.FindElement(searchResultsPage.SearchResultsContainer);
            Assert.IsTrue(searchResults.WeElementIsDisplayed());
            searchResultsPage.SelectFirstListedSearchResult();
        }

        [Then(@"they see the Reddit homepage")]
        public void ThenTheySeeTheRedditHomePage()
        {
            IWebElement redditPageContent = redditPage.driver.FindElement(redditPage.redditContentArea); 
            Assert.IsTrue(redditPageContent.WeElementIsEnabled());
        }
    }
}