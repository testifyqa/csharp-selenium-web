using ProductAutomation.Pages;
using TechTalk.SpecFlow;

namespace ProductAutomation.Steps.SearchSteps
{
    [Binding]
    public class SearchSteps
    {
        private BasePage basePage = new BasePage();

        [When(@"they search for ""(.*)""")]
        public void WhenISearchFor(string searchTerm)
        {
            basePage.SearchFor(searchTerm);
        }
    }
}